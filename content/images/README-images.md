La idea es almacenar aquí algunas imágnes estáticas: en FiQuiPedia hay pocas imágenes almacenadas localmente, casi siempre se enlaza a la fuente.

* Icono ficheros pdf: iconopdf.png
Antes de 2021 se usaba https://upload.wikimedia.org/wikipedia/commons/2/22/Pdf_icon.png  
En 2021 se usa https://upload.wikimedia.org/wikipedia/commons/8/87/PDF_file_icon.svg  
Una idea sería en todos los ficheros referenciar /images/iconopdf (sin extensión) y así se actualizaría en un único sitio  
ToDo por revisar, no funciona en local ¿por extensión?

* Icono ficherow word
Inicialmente ponía http://upload.wikimedia.org/wikipedia/commons/thumb/8/88/MS_word_DOC_icon.svg/46px-MS_word_DOC_icon.svg.png 
En 2021 en la migración veo que no funciona y uso https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/MS_word_DOC_icon_(2003-2007).svg/46px-MS_word_DOC_icon_(2003-2007).svg.png
Se podría intentar poner un icono general de documento, no asociado a formato

* Logo FiQuiPedia: 2017-06-23-LogoFiquipedia.png
Antes migración Google Sites lo almacenaba en _/rsrc/1498312812328/home/2017-06-23-LogoFiquipedia3eaf.png  
En migración tema hugo lo usa en themes/beautifulhugo/static/img/img/avatar-icon.png  
Se usa en config.toml desde [Params]    logo = "img/avatar-icon.png"

* Icono para web: favicon.ico
Antes migración Google Sites lo almacenaba en  _/rsrc/1528405774924/favicon.png  
En migración tema hugo lo usa en themes/beautifulhugo/static/img/favicon.ico  
Se usa en config.toml desde [Params]  favicon = "img/favicon.ico"

* Código QR FiQuiPedia: FiquipediaQR.png
Antes migración Google Sites lo almacenaba en _/rsrc/1411658790825/home/FiquipediaQRca9d.png

* Logo Real Sociedad Española de Química 
Antes migración Google Sites lo almacenaba en _/rsrc/1466526534242/home/logo-rseq-20167d02.jpg
En migración se usa https://rseq.org/wp-content/uploads/2020/05/2020_logorseqlargo_transp.png

* Captura dibujos animados Dennis Gnasher: ReaccionesQuímicasDennisGnasher.png
Antes migración Google Sites lo almacenaba en  _/rsrc/1609430371289/home/recursos/quimica/recursos-reacciones-y-calculos-estequiometricos/DennisGnasher.png


